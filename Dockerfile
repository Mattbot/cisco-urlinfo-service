FROM quay.io/mattbot/base-ruby:latest

RUN apt-get -y update \
  && apt-get -y --no-install-recommends install \
      build-essential \
      libmysqlclient-dev \
      libcurl4-openssl-dev \
      nodejs \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /gems

WORKDIR /app/

COPY Gemfile Gemfile.lock /app/

RUN BUNDLE_IGNORE_MESSAGES=true bundle install --jobs $(getconf _NPROCESSORS_ONLN)

# Provide *default* values for some of our configuration variables:
ENV \
  CI=false \
  DEV_MODE=false \
  LOG_LEVEL=info \
  RAILS_CACHING=false \
  RAILS_FORCE_SSL=false \
  RAILS_LOG_TO_STDOUT=true \
  RAILS_SERVE_STATIC_FILES=true \
  PATH=/app/bin:$PATH

ENV PATH="/app/bin:$PATH"

COPY ./app/ /app/app
COPY ./bin/ /app/bin
COPY ./config/ /app/config
COPY ./lib/ /app/lib
COPY ./vendor/ /app/vendor
COPY Rakefile /app/

# RUN /app/bin/rake --trace assets:precompile \
#  && rm -rf tmp/* log/*

COPY . /app/

# precompile assets (for ActiveAdmin) -- the faking of env variables is a necessary evil until a better solution comes along
RUN DATABASE_URL=mysql2://fake \
    DEV_MODE=true \
    EXTERNAL_URL=http://fake \
    RAILS_ASSETS_PRECOMPILE=true \
    SECRET_KEY_BASE=fake \
    HMAC_SECRET=fake \
    rake assets:precompile --trace \
 && rm -rf tmp/*

ENTRYPOINT ["/sbin/tini", "-vvg", "--", "/app/docker/entrypoint.sh"]

# EXPOSE 3101

STOPSIGNAL SIGINT

# CMD ["puma", "--config", "config/puma.rb", "-p", "3101"]
CMD ["puma", "--config", "config/puma.rb"]

