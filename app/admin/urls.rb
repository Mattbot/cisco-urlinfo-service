ActiveAdmin.register Url do
  permit_params :url, :user_id

  index do
    selectable_column
    id_column
    column :url
    column :user_id
    actions
  end

  form do |f|
    f.inputs do
      f.input :url
      f.input :user_id
    end
    f.actions
  end
end

