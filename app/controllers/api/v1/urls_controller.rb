class Api::V1::UrlsController < Api::V1::ApplicationController
  before_action :authenticate, only: %i[new create update destroy]
  before_action :set_url, only: %i[show update destroy]

  # GET /url
  def index
    @url = Url.all
    render json: @url
  end

  # GET /url/1
  def show
    @url.nil? ? not_found : render(json: @url)
  end

  # POST /url
  def create
    @url = Url.new(url_params_with_token_user_id)

    if @url.save
      render json: @url, status: :created, location: api_v1_url_url(@url)
    else
      render json: @url.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /url/1
  def update
    if @url.update(url_params_with_token_user_id)
      render json: @url
    else
      render json: @url.errors, status: :unprocessable_entity
    end
  end

  # DELETE /url/1
  def destroy
    requesting_user_id == @url.user_id ? @url.destroy : render_auth_failure
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_url
    @url = Url.find_by(id: params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def url_params
    params.permit(:url)
  end

  def url_params_with_token_user_id
    url_params.merge(user_id: requesting_user_id)
  end
end
