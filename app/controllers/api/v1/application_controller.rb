class Api::V1::ApplicationController < ActionController::API
  private

  def authenticate
    return render_auth_failure unless auth_header?
    valid_token? ? true : render_auth_failure
  end

  def render_auth_failure
    render(json: { 'user': 'unauthorized' }, status: :unauthorized)
    false
  end

  def auth_header?
    auth_header.nil? ? false : true
  end

  def auth_header
    header = request.headers['Authorization']
    header ? header.sub(/Bearer /, '') : nil
  end

  def valid_token?
    begin
      decode_token
    rescue
      # Better to catch all Errors than please Rubocop.
      return false
    end
    true
  end

  def decode_token
    JWT.decode(
      auth_header,
      hmac_secret,
      true,
      algorithm: 'HS256'
    )
  end

  def hmac_secret
    Rails.application.secrets.hmac_secret
  end

  def requesting_user_id
    decode_token[0]['user_id']
  end

  def no_content
    head 204, 'content_type' => 'text/plain'
  end

  def not_found
    head 404, 'content_type' => 'text/plain'
  end
end
