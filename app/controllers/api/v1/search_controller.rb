class Api::V1::SearchController < Api::V1::ApplicationController
  # GET /search/?query='url'
  def search
    @url = Url.find_by(url: CGI.unescape(params[:query]))
    @url.nil? ? no_content : render(json: @url)
  end
end
