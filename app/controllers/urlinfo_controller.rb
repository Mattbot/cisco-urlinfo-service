class UrlinfoController < ApplicationController
  # GET /urlinfo/1/:hostname_port}/:path_and_query}
  # GET /urlinfo/1/cnn.com:80/2018/09/12/us/hurricane/index.html?why=dontknow}

  # We should have a talk about the many problems with this API design.
  # For a cleaner implementation, see this app at:
  # # GET /api/v1/search/?query='url'
  # http://www.ietf.org/rfc/rfc3986.txt

  def search
    query = parse_url_query(params)
    @url = Url.where(url: query)
    @url = @url.exists? ? @url : check_for_url_queries(query)
    url_existance = @url.exists?
    url_existance ? render(json: @url) : no_content
  end

  private

  def parse_url_query(params)
    # {"q=>a", "host"=>"cnn.com", "port"=>"80", "path"=>"asds/dsSASD/DD", "format"=>"HTML"}
    # The query section of the URL is very hard to recontruct
    # after passing through the Rails router.
    scheme = determine_scheme(params[:port])
    host = params[:host]
    path = params[:path]
    format_fragment = params[:format] ? ".#{params[:format]}" : nil
    "#{scheme}://#{host}/#{path}#{format_fragment}"
  end

  def determine_scheme(port)
    port == '443' ? 'https' : 'http'
  end

  def check_for_url_queries(query)
    Url.where("url LIKE '#{query}?%'")
  end
end
