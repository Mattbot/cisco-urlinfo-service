class ApplicationController < ActionController::Base
  def no_content
    head 204, 'content_type' => 'text/plain'
  end
end
