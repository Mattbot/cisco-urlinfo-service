require 'test_helper'

class Api::V1::SearchControllerTest < ActionDispatch::IntegrationTest
  setup do
    # @url = url(:one)
  end

  test 'should find url in search' do
    params = { query: 'http://found.com' }
    get api_v1_search_url(params: params)
    assert_response :success
  end

  test 'should not find url in search' do
    params = { query: 'http://notfound.com' }
    get api_v1_search_url(params: params)
    assert_response 204
  end
end
