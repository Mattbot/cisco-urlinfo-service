require 'test_helper'

class Api::V1::UrlsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @url = url(:one)
  end

  def authenticated_header
    hmac_secret = Rails.application.secrets.hmac_secret
    token = JWT.encode({ user_id: 1 }, hmac_secret, 'HS256')
    { 'Authorization': "Bearer #{token}" }
  end

  def authenticated_header_u23
    hmac_secret = Rails.application.secrets.hmac_secret
    token = JWT.encode({ user_id: 23 }, hmac_secret, 'HS256')
    { 'Authorization': "Bearer #{token}" }
  end

  def bad_authenticated_header
    hmac_secret = 'bad'
    token = JWT.encode({ user_id: 1 }, hmac_secret, 'HS256')
    { 'Authorization': "Bearer #{token}" }
  end

  test 'should get index' do
    get(api_v1_urls_url, as: :json)
    assert_response :success
  end

  test 'should create url' do
    assert_difference('Url.count') do
      params = {
        url: {
          url: @url.url,
          user_id: @url.user_id
        }
      }
      post(
        api_v1_urls_url,
        params: params,
        headers: authenticated_header,
        as: :json
      )
    end

    assert_response 201
    assert JSON.parse(response.body)['user_id'] == 1
  end

  test 'should show url' do
    get api_v1_url_url(@url), as: :json
    assert_response :success
  end

  test 'should update url' do
    params = {
      url: {
        url: @url.url,
        user_id: @url.user_id
      }
    }
    patch(
      api_v1_url_url(@url),
      headers: authenticated_header,
      params: params,
      as: :json
    )
    assert_response 200
  end

  test 'should destroy url' do
    assert_difference('Url.count', -1) do
      delete(api_v1_url_url(@url), headers: authenticated_header, as: :json)
    end
    assert_response 204
  end

  test 'should not destroy url without token' do
    delete(api_v1_url_url(@url), as: :json)
    assert_response 401
  end

  test 'should not destroy url with bad token' do
    delete(api_v1_url_url(@url), headers: bad_authenticated_header, as: :json)
    assert_response 401
  end

  test 'should not destroy url with wrong user' do
    delete(api_v1_url_url(@url), headers: authenticated_header_u23, as: :json)
    assert_response 401
  end
end
