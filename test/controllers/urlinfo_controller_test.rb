require 'test_helper'

class UrlinfoControllerTest < ActionDispatch::IntegrationTest
  setup do
    @url = url(:one)
  end

  test 'should find url in search' do
    get('http://0.0.0.0:34255/urlinfo/1/found.com:80')
    assert_response :success
  end

  test 'should find url with path in search' do
    get('http://0.0.0.0:34255/urlinfo/1/found.com:80/asd')
    assert_response :success
  end

  test 'should find url with format in search' do
    get('http://0.0.0.0:34255/urlinfo/1/found.com:80/asd.html')
    assert_response :success
  end

  test 'should find url with query in search' do
    get('http://0.0.0.0:34255/urlinfo/1/found.com:80/asd.html?q=a')
    assert_response :success
  end

  test 'should not find url in search' do
    get('http://0.0.0.0:34255/urlinfo/1/notfound.com:80/asd/asd')
    assert_response 204
  end
end
