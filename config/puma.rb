max_threads = ENV.fetch('RAILS_MAX_THREADS') { 32 }.to_i
min_threads = ENV.fetch('RAILS_MIN_THREADS') { 1 }.to_i
threads min_threads, max_threads

bind 'tcp://0.0.0.0:3000'
bind 'unix:///app/tmp/sockets/puma.sock'

environment ENV.fetch('RAILS_ENV') { 'development' }

if ENV.fetch('DEV_MODE', 'false') == 'true'
  activate_control_app 'unix:///app/tmp/sockets/pumactl.sock', no_token: true
end
