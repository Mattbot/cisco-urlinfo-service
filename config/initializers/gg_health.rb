require 'goodguide/health'

Goodguide::Health.configure do |health|
  health.revision = ENV['GIT_REVISION']
end
