Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'api/v1/urls#index'

  namespace :api do
    namespace :v1 do
      resources :urls
      get '/search', to: 'search#search'
    end
  end

  # GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}
  get '/urlinfo/1/*host:*port/(*path)', to: 'urlinfo#search'

  mount Goodguide::Health.app, at: '/health', as: 'health'
  get '/' => redirect('/health/status')
end
