# Cisco URLInfo Service

## Obective
> For this exercise we would like to see how you go about solving a rather straight forward coding challenge and architecting for the future. One of our key values in how we develop new systems is to start with very simple implementations and progressively make them more capable, scalable and reliable. And releasing them each step of the way. As you work through this exercise it would be good to "release" frequent updates by pushing updates to a shared git repo (we like to use Bitbucket's free private repos for this). It's up to you how frequently you do this and what you decide to include in each push. Don't forget some unit tests (at least
something representative).

> Here's what we would like you to build"

>### URL lookup service
> We have an HTTP proxy that is scanning traffic looking for malware URLs. Before allowing HTTP connections to be made, this proxy asks a service that maintains several databases of malware URLs if the resource being requested is known to contain malware.

>Write a small web service, in the language/framework your choice, that responds to GET requests where the caller passes in a URL and the service responds with some information about that URL.

>The GET requests look like this:

```GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}```

>The caller wants to know if it is safe to access that URL or not. As the implementer you get to choose the response format and structure. These lookups are blocking users from accessing the URL until the caller receives a response from your service.
Give some thought to the following:

  > * The size of the URL list could grow infinitely, how might you scale this beyond the memory capacity of this VM? Bonus if you implement this.
  > * The number of requests may exceed the capacity of this VM, how might you solve that? Bonus if you implement this.
  > * What are some strategies you might use to update the service with new URLs? Updates may be as much as 5 thousand URLs a day with updates arriving every 10 minutes.

> Email us your ideas.

## Implementation
This service API was built in Ruby 2.5.1 using Ruby on Rails 5.2.1 and uses MySQL 5.7 for data storage. Requests to the app are proxied through an Nginx server.

### Features

> The size of the URL list could grow infinitely, how might you scale this beyond the memory capacity of this VM? Bonus if you implement this.

Data storage is handled via a MySQL database.

> The number of requests may exceed the capacity of this VM, how might you solve that? Bonus if you implement this.

This app is intended to be hosted using the AWS cloud.  Application response time and request capacity is first addressed locally via an Nginx proxy service.  The app infrastruct then handles auto-scale on two different levels via AWS ELB and ECS.  At the ECS task level, the auto-scaler may increase the number of application instances running based on the CPU and memory usage of the host server.  At the ECS cluster level, the number of EC2 instance members of the cluster can be auto-scaled based on CPU and memory usage.  Finally, there is a caching and CDN provided by AWS CloudFront. See [https://gitlab.com/Mattbot/demo-infrastructure](https://gitlab.com/Mattbot/demo-infrastructure) for details.

> What are some strategies you might use to update the service with new URLs? Updates may be as much as 5 thousand URLs a day with updates arriving every 10 minutes.

The service has a fully implement RESTful CRUD API interface for Url data projected with [JSON Web Tokens](https://jwt.io).

### API Request
```GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}```

### API Response

### On match:
HTTP Response: 200

Body: JSON array of matched URL objects

Example:
```
[
  {
    "created_at": "2018-09-13T02:51:47.000Z",
    "id": 5,
    "updated_at": "2018-09-13T02:52:19.000Z",
    "url": "http://cnn.com/2018/09/12/us/hurricane/index.html?something=else",
    "user_id": null
  },
  ...
]
```

### On NO match:
HTTP Response: 204

Body: none

## Improved API Design

The API request in the spefications has major design flaws and a new request API endpoint has been implemented to address them.

The two major design flaws with the specification request are:
  1. The request 'query' does not contain a complete or valid URI as per [RFC 3986 Uniform Resource Identifier (URI): Generic Syntax](http://www.ietf.org/rfc/rfc3986.txt)
  2. The entire specification request format and 'query' is a valid (yet poorly formed) URI in itself, making parsing the service namespace path from the intented URI query unneccessarily ambigious.

The improved design addresses both issues in a cleanly versioned API namespace.

### Improved API Request

```GET /api/v1/search?query=<URL>```

### Improved API Response

Same as [above](#api-response)

### Developer Workflow

The developer workflow for this project is Docker based.  You must have access to a local or remote Docker host to build and develop on the project.  Local installation of tools such as Ruby, Rails, MySQL, etc. are not required.  See notes on [Docker Developer Workflow](https://gitlab.com/Mattbot/docs/wikis/docker-developer-workflow) for more information.

#### Local Development
Rails, MySQL, and Nginx all run inside Docker containers.

##### Developer Setup Instructions
Setup Docker-compose overrides file:
```
cp docker-compose.override.example.yml docker-compose.override.yml
```

For the first run, setup the MySQL database:
```
docker-compose run app rake db:setup db:migrate
```

To start the application, run the following commands:
```
docker-compose build
docker-compose up
```

Once running, the development nginx proxy server can be reached at [http://0.0.0.0:34255](http://0.0.0.0:34255)

To run tests:
```
docker-compose run test rake db:setup db:migrate
docker-compose run test rake test
```

#### Remote Development
Rails and Nginx are run in Docker containers in a cloud AWS ECS cluster.  The app Docker image is stored in a AWS ECS Repository.  An AWS RDS MySQL instance is used for data storage.

To build and push a new image to the ECS repo, setup up the AWS CLI and then run the following command:
```
./devtools/bin/build-and-push-for-deployment
```

Infrastructure and deployment for this project is managed using Terraform: [https://gitlab.com/Mattbot/demo-infrastructure](https://gitlab.com/Mattbot/demo-infrastructure)

##### Online Demo

[https://cisco-urlinfo-service.mattbot.net](https://cisco-urlinfo-service.mattbot.net)


