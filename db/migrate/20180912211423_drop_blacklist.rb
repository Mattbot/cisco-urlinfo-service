class DropBlacklist < ActiveRecord::Migration[5.2]
  def self.up
    drop_table :blacklist
  end

  def self.down
    create_table :blacklist do |t|
      t.string      :url
      t.integer     :user_id
      t.string      :hostname
      t.integer     :port
      t.string      :path
      t.string      :params

      t.timestamps
    end
    add_index :blacklist, :url
  end
end
